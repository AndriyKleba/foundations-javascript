// Number
// const num = 42; //Integer
// console.log(42); // Float
// const pow = 10e3;
// const float = 42.12

// Number.MAX_SAFE_INTEGER;
// console.log(Math.pow(2, 53) - 1);
// console.log(Number.MAX_SAFE_INTEGER);

// console.log(Number.MAX_VALUE);
// console.log(Number.MIN_VALUE);
// console.log(Number.POSITIVE_INFINITY);
// console.log(Number.NEGATIVE_INFINITY);
// console.log(Number.NaN);
// console.log(2 / "calc");
// console.log(typeof NaN);

// const stringInt = "42";
// const stringFloat = "42.42";
// console.log(stringInt + 2);
// console.log(Number.parseInt(stringInt));
// console.log(+stringInt);
// console.log(parseFloat(stringFloat));
// console.log(+stringFloat);

// console.log(0.4 + 0.2);
// console.log(+(0.4 + 0.2).toFixed(1));

// BigInt
// console.log(typeof 9007199254740991);
// console.log(typeof 9007199254740991n);

// console.log(10n - 4n);
// console.log(BigInt(12) - 5n);

// Math

// console.log(Math.max(2, 2, 3, 4, 5));
// console.log(Math.pow(2, 3));
// console.log(MAth.floor(2.2));
// console.log(MAth.round(2.2));
// console.log(MAth.trunc(2.2));
// console.log(MAth.ceil(2.2));

// console.log(Math.random());

// 4 Example

// function getRandom(min, max) {
//   const num = Math.random() * (max - min + 1) + min;

//   return Math.floor(num);
// }

// console.log(getRandom(10, 42));
