const person = {
  name: "Oleg",
  age: 23,
  isWork: true,
  lang: ["ua", "ru", "de"],
  "complex key": "ValueKey",
  great() {
    return console.log(`Hello ${this.name}`);
  },
};

person.great();
