// browser-sync start --server --files "**/*" --no-notify
// // 1 Зміні

// // variable
// var name = "Andrii";
// // const lastName = "Kleba"; //Srtring
// const age = 24; //Number
// let isDevelop = true; //Boolean
// name = "Andr";

// const _private = "private";
// const $ = "$";
// // const if
// const withNum = 5;

// // 2 Мутація
// // ""; '' ``
// // console.log(`Name human ${name}`);
// console.log(`Name human ${name}`);
// const lastName = prompt("Введіть прізвище");
// alert(lastName);

// 3) Оператори
// const currentYear = 2020;
// const birthYear = 1995;

// const age = currentYear - birthYear;
// console.log(age);

// const a = 10;
// let b = 15;
// // console.log(a + b);
// // console.log(a - b);
// // console.log(a * b);
// // console.log(a / b);

// // console.log(b++);
// // console.log(++b);
// // console.log(b);

// let c = 32;
// // c += a;
// // c -= a;
// // c *= a;
// // c /= a;

// console.log(c);

// 4) Типи Даних
// const isDevelop = true;
// const name = "Andrii";
// const age = 24;
// let x;
// null

// console.log(typeof isDevelop);
// console.log(typeof name);
// console.log(typeof age);
// console.log(typeof x);
// console.log(typeof null);

//  Пріорітет операторів

// const fullAge = 24;
// const birthYear = 1995;
// const currentYear = 2020;

//  > < >= <=
// const isFullAge = (currentYear - birthYear) >= fullAge;
// const isFullAge = currentYear - birthYear >= fullAge;

// Умовні оператори

// const server = "pending";

// if (server === "pending") {
//   console.log("server is running");
// } else if (server === "fail") {
//   console.log("server is stop");
// } else {
//   console.log("what");
// }

// const num1 = 41;
// const num2 = "41";

// console.log(num1 == num2);
// console.log(num1 === num2);

// const isReady = true;
// if (isReady) {
//   console.log("go");
// }
// if (isReady === true) {
//   console.log("go");
// }

// Тернарний вираз
// isReady ? console.log('true'); : console.log('false');

// 7 Булева логіка

// true && true
// true && false
// false && true
// false && false

// true || true
// true || false
// false || true
// false || false

// true
// !true
// !!true
// !false

//  8 Функції

// function calcAge(year) {
//   return 2020 - year;
// }

// calcAge(1956);
// const age = calcAge(1956);

// function logInfoPerson(name, year) {
//   const age = calcAge(year);

//   return console.log(name, age);
// }

// logInfoPerson("Andrii", 24);

// 9 Масиви
// const cars = ["Audi", "BMW", "Ford"];
// console.log(cars);

// const cars = new Array("Audi", "BMW", "Ford");
// console.log(cars);

// console.log(cars[2]);
// console.log(cars.length);
// cars[0] = "Porshe";
// console.log(cars);
// cars[3] = "Audi";
// cars[cars.length] = "Lada";
// console.log(cars);

// 10 Цикли
// const cars = ["Audi", "BMW", "Ford"];
// for (let i = 0; i < cars.length; i++) {
//   //   console.log(i, cars);
//   let car = cars[i];
//   console.log(car);
// }

// for (let car of cars) {
//   console.log(car);
// }

// 10 Обєкти

// const person = new Object({name: Andrii});
// const person = {
//   firstName: "Andrii",
//   lastName: "Kleba",
//   age: 24,
//   lang: ["Dart", "JS", "Java"],
//   hi: function () {
//     console.log("Hello");
//   },
// };

// person.hi();
// console.log(person.firstName);
// console.log(person["lastName"]);
// // const year = person.lang;
// // console.log(year);
// const year = "lang";
// console.log(person[year]);
// person.hasWife = true;

// console.log(person.hasWife);
