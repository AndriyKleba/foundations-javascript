// const timeOut = setTimeout(() => {
//   console.log("After timeOut");
// }, 1000);

// clearTimeout(timeOut);

const delay = (wait = 1000) => {
  const promise = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve();
    }, wait);
  });
  return promise;
};

delay(2000)
  .then(() => console.log("After two seconds"))
  .catch((err) => console.log(err))
  .finally(() => console.log("Finish"));
