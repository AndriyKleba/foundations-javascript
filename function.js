//  Функція

// Function Declaration
// function greet(name) {
//   console.log("Hi " + name);
// }

// greet("Orest");

// Function Expression
// const orest = function greet(name) {
//   console.log("Hi " + name);
// };

// greet("Orest");

// const orest = function (name) {
//   console.log("Hi " + name);
// };

// greet("Orest");

// Анонімні функції
// let counter = 0;

// const interval =  setInterval(function () {
// if (counter === 5) {
//   clearInterval(interval);
// }else
//   console.log(++counter);
// }, 1000);

// 3 Arrow function
// const arrow = (name) => console.log(name);

// 4 Параметри по замовчувані
// const sum = (a, b = 2) => a + b;

// console.log(sum(1));

// function sumAll(...all) {
//   console.log(all);
// }

// Замиканя

// function createMember(name, lastName) {
//   return function () {
//     console.log(name + " " + lastName);
//   };
// }

// const result = createMember("Andrii", "Kleba");
// console.log(result());
