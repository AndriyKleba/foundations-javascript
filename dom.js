// document
// window
// window.alert('1');

const head = document.getElementById("hello");
console.dir(head);

setTimeout(() => {
  const a = (head.textContent = "Good idea");
  head.style.color = "red";
}, 1000);

head.onclick = () => {
  if ((head.style.color = "red")) {
    head.textContent = "Bad idea";
    head.style.backgroundColor = "black";
  }
};
